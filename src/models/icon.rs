use serde_derive::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Icon {
    pub host: String,
    pub url: String,
}

impl Icon {
    pub fn decompose(self) -> (String, String) {
        (
            self.host,
            self.url,
        )
    }
}
