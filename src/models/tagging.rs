use crate::{FeedID, TaggingID};
use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Deserialize)]
pub struct Tagging {
    pub id: TaggingID,
    pub feed_id: FeedID,
    pub name: String,
}

impl Tagging {
    pub fn decompose(self) -> (TaggingID, FeedID, String) {
        (
            self.id,
            self.feed_id,
            self.name
        )
    }
}

#[derive(Debug, Serialize)]
pub struct CreateTaggingInput {
    pub feed_id: FeedID,
    pub name: String,
}

#[derive(Debug, Serialize)]
pub struct RenameTagInput {
    pub old_name: String,
    pub new_name: String,
}

#[derive(Debug, Serialize)]
pub struct DeleteTagInput {
    pub name: String,
}
