use failure::{Backtrace, Context, Fail};
use std::fmt;

#[derive(Debug)]
pub struct ApiError {
    inner: Context<ApiErrorKind>,
}

#[derive(Clone, Eq, PartialEq, Debug, Fail)]
pub enum ApiErrorKind {
    #[fail(display = "Server returned invalid response")]
    ServerIsBroken,
    #[fail(display = "A network error occurred")]
    Network,
    #[fail(display = "Invalid username or password")]
    InvalidLogin,
    #[fail(display = "Access denied")]
    AccessDenied,
    #[fail(display = "Input exceeds max size")]
    InputSize,
    #[fail(display = "Invalid cache response")]
    InvalidCaching,
}

impl Fail for ApiError {
    fn cause(&self) -> Option<&dyn Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.inner.backtrace()
    }
}

impl fmt::Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.inner, f)
    }
}

impl ApiError {
    pub fn kind(&self) -> ApiErrorKind {
        self.inner.get_context().clone()
    }
}

impl From<ApiErrorKind> for ApiError {
    fn from(kind: ApiErrorKind) -> ApiError {
        ApiError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<ApiErrorKind>> for ApiError {
    fn from(inner: Context<ApiErrorKind>) -> ApiError {
        ApiError { inner }
    }
}
